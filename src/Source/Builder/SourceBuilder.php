<?php
declare(strict_types=1);

namespace App\Source\Builder;

use App\Source\Entity\FileSourceEntity;
use App\Source\Entity\SourceReadableInterface;
use App\Source\Entity\SourceWritableInterface;

class SourceBuilder
{
    private const WRITABLE_DIR = 'output';

    /**
     * @param string $name
     * @return FileSourceEntity
     */
    static public function getReadableFile(string $name): SourceReadableInterface
    {
        return new FileSourceEntity($name, 'r');
    }

    /**
     * @param string $name
     * @return FileSourceEntity
     */
    static public function getWritableFile(string $name): SourceWritableInterface
    {
        return new FileSourceEntity(self::WRITABLE_DIR . DIRECTORY_SEPARATOR . $name, 'w');
    }
}