<?php
declare(strict_types=1);

namespace App\Source\Entity;

interface SourceWritableInterface extends SourceInterface
{
    /**
     * @param string $line
     */
    public function setLine(string $line): void;
}