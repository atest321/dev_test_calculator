<?php
declare(strict_types=1);

namespace App\Source\Entity;

class FileSourceEntity implements SourceWritableInterface, SourceReadableInterface
{
    private $handler;

    /**
     * @param string $name
     * @param string $mode
     */
    public function __construct(string $name, string $mode)
    {
        $this->handler = fopen(ROOT . $name, $mode);
    }

    /**
     * @inheritdoc
     */
    public function getLine()
    {
        return fgets($this->handler, 4056);
    }

    /**
     * @inheritdoc
     */
    public function setLine(string $line): void
    {
        fputs($this->handler, $line . PHP_EOL);
    }

    public function close(): void
    {
        fclose($this->handler);
    }
}