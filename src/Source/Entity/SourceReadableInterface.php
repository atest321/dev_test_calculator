<?php
declare(strict_types=1);

namespace App\Source\Entity;

interface SourceReadableInterface extends SourceInterface
{
    /**
     * @return string|bool
     */
    public function getLine();
}