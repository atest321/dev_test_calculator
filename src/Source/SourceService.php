<?php
declare(strict_types=1);

namespace App\Source;

use App\Source\Builder\SourceBuilder;
use App\Source\Entity\FileSourceEntity;
use App\Source\Entity\SourceInterface;
use Generator;

class SourceService
{
    private const PREFIX_LOG = 'log_';
    private const PREFIX_RESULT = 'result_';

    /**
     * @param string $name
     * @return bool
     */
    public function isSourceAvailable(string $name)
    {
        return file_exists($name);
    }

    /**
     * @param string $name
     * @return FileSourceEntity
     */
    public function getReadSource(string $name): SourceInterface
    {
        return SourceBuilder::getReadableFile($name);
    }

    /**
     * @param string $name
     * @return FileSourceEntity
     */
    public function getLogSource(string $name): SourceInterface
    {
        return SourceBuilder::getWritableFile(self::PREFIX_LOG . $name);
    }

    /**
     * @param string $name
     * @return FileSourceEntity
     */
    public function getResultSource(string $name): SourceInterface
    {
        return SourceBuilder::getWritableFile(self::PREFIX_RESULT . $name);
    }

    /**
     * @param string $fileName
     * @return Generator
     */
    public function getValues(string $fileName)
    {
        $source = $this->getReadSource($fileName);
        while ($buffer = $source->getLine()) {
            yield $buffer;
        }
        $source->close();
    }
}