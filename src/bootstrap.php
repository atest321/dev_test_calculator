<?php
declare(strict_types=1);

use App\Calculator\CalculatorService;
use App\Calculator\Helper\ConsoleParamsHelper;
use App\Source\SourceService;
use App\MathCommand\CommandService;
use App\MathCommand\Mapper\CommandMapper;

require __DIR__ . '/../vendor/autoload.php';

/**
 * @return CalculatorService
 */
function getCalculatorService(): CalculatorService
{
    $sourceService = new SourceService();
    $consoleParamsHelper = new ConsoleParamsHelper();

    $commandMapper = new CommandMapper();
    $commandService = new CommandService($commandMapper);
    return new CalculatorService($sourceService, $commandService, $consoleParamsHelper);
}
