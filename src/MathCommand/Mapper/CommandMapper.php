<?php
declare(strict_types=1);

namespace App\MathCommand\Mapper;

use App\MathCommand\Entity\CommandInterface;
use App\MathCommand\Entity\DivisionEntity;
use App\MathCommand\Entity\MinusEntity;
use App\MathCommand\Entity\MultiplyEntity;
use App\MathCommand\Entity\PlusEntity;

class CommandMapper
{
    private const COMMANDS_MAP = [
        'plus' => PlusEntity::class,
        'minus' => MinusEntity::class,
        'multiply' => MultiplyEntity::class,
        'division' => DivisionEntity::class,
    ];

    /**
     * @param string $commandName
     * @return bool
     */
    public function isCommandExists(string $commandName): bool
    {
        return in_array($commandName, array_keys(self::COMMANDS_MAP), true);
    }

    /**
     * @param string $commandName
     * @return CommandInterface
     */
    public function getCommandObjectByName(string $commandName): CommandInterface
    {
        $commandClass = self::COMMANDS_MAP[$commandName];
        return new $commandClass();
    }
}