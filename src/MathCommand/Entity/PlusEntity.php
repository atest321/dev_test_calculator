<?php
declare(strict_types=1);

namespace App\MathCommand\Entity;

class PlusEntity extends AbstractCommandEntity
{
    /** @inheritdoc */
    public function result(int $firstValue, int $secondValue): int
    {
        return $firstValue + $secondValue;
    }
}