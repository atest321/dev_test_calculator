<?php
declare(strict_types=1);

namespace App\MathCommand\Entity;

abstract class AbstractCommandEntity implements CommandInterface
{
    /** @var string */
    protected $message = '';

    /** @inheritdoc */
    public function isValidValues($a, $b): bool
    {
        $flag = true;
        if (!is_numeric($a) || !is_numeric($b)) {
            $this->message = sprintf('set of numbers %d and %d is wrong, bad type ', $a, $b);
            $flag = false;
        }

        return $flag;
    }

    /** @inheritdoc */
    public function getMessage(): string
    {
        return $this->message;
    }

    /** @inheritdoc */
    abstract public function result(int $firstValue, int $secondValue);
}