<?php
declare(strict_types=1);

namespace App\MathCommand\Entity;

interface CommandInterface
{
    /**
     * @param int $firstValue
     * @param int $secondValue
     * @return int|float
     */
    public function result(int $firstValue, int $secondValue);

    /**
     * @param $firstValue
     * @param $secondValue
     * @return bool
     */
    public function isValidValues($firstValue, $secondValue): bool;

    /**
     * @return string
     */
    public function getMessage(): string;
}