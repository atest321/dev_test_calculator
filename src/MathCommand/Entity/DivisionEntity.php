<?php
declare(strict_types=1);

namespace App\MathCommand\Entity;

class DivisionEntity extends AbstractCommandEntity
{
    /** @inheritdoc */
    public function result(int $firstValue, int $secondValue): float
    {
        return $firstValue / $secondValue;
    }

    /** @inheritdoc */
    public function isValidValues($firstValue, $secondValue): bool
    {
        $flag = parent::isValidValues($firstValue, $secondValue);

        if ($flag && $secondValue === 0) {
            $this->message = sprintf('set of numbers %d and %d is wrong, second value cannot be null', $firstValue, $secondValue);
            $flag = false;
        }

        return $flag;
    }
}