<?php
declare(strict_types=1);

namespace App\MathCommand;

use App\MathCommand\Entity\CommandInterface;
use App\MathCommand\Exception\MathCommandException;
use App\MathCommand\Mapper\CommandMapper;

class CommandService
{
    /** @var CommandMapper */
    private $commandMapper;

    public function __construct(CommandMapper $commandMapper)
    {
        $this->commandMapper = $commandMapper;
    }

    /**
     * @param string $action
     * @return CommandInterface
     * @throws MathCommandException
     */
    public function getCommandByAction(string $action): CommandInterface
    {
        $action = strtolower(trim($action));
        if (!$this->commandMapper->isCommandExists($action)) {
            throw new MathCommandException('Unknown command for action: ' . $action);
        }
        return $this->commandMapper->getCommandObjectByName($action);
    }
}