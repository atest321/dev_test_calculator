<?php
declare(strict_types=1);

namespace App\Calculator;

use App\Calculator\Entity\CalculatorParameters;
use App\Calculator\Exception\CalculatorException;
use App\Calculator\Helper\ConsoleParamsHelper;
use App\Source\Entity\SourceWritableInterface;
use App\MathCommand\Entity\CommandInterface;
use App\MathCommand\Exception\MathCommandException;
use App\Source\SourceService;
use App\MathCommand\CommandService;

class CalculatorService
{
    /** @var SourceService */
    private $sourceService;
    /** @var CommandService */
    private $commandService;
    /** @var ConsoleParamsHelper */
    private $consoleHelper;

    /**
     * @param SourceService $sourceService
     * @param CommandService $commandService
     * @param ConsoleParamsHelper $consoleParamsHelper
     */
    public function __construct(
        SourceService $sourceService,
        CommandService $commandService,
        ConsoleParamsHelper $consoleParamsHelper
    )
    {
        $this->sourceService = $sourceService;
        $this->commandService = $commandService;
        $this->consoleHelper = $consoleParamsHelper;
    }

    /**
     * @throws CalculatorException
     * @throws MathCommandException
     */
    public function calculate()
    {
        list($action, $fileName) = $this->getParameters();

        if (!$this->sourceService->isSourceAvailable($fileName)) {
            throw new CalculatorException('Source file is not available');
        }

        /** @var CommandInterface $command */
        $command = $this->commandService->getCommandByAction($action);
        $log = $this->sourceService->getLogSource($fileName);
        $result = $this->sourceService->getResultSource($fileName);

        $this->calculateAndSave($fileName, $command, $log, $result);

        $result->close();
        $log->close();
    }

    /**
     * @return array
     * @throws CalculatorException
     */
    private function getParameters(): array
    {
        $this->consoleHelper->init([
            CalculatorParameters::PARAMETER_ACTION,
            CalculatorParameters::PARAMETER_FILE
        ]);
        $action = $this->consoleHelper->getValueByParameterNames(CalculatorParameters::PARAMETER_ACTION);
        if (!$action) {
            throw new CalculatorException('Action is not provided');
        }

        $fileName = $this->consoleHelper->getValueByParameterNames(CalculatorParameters::PARAMETER_FILE);
        if (!$fileName) {
            throw new CalculatorException('File name is not provided');
        }
        return [$action, $fileName];
    }

    /**
     * @param string $fileName
     * @param CommandInterface $command
     * @param SourceWritableInterface $log
     * @param SourceWritableInterface $result
     */
    private function calculateAndSave(
        string $fileName,
        CommandInterface $command,
        SourceWritableInterface $log,
        SourceWritableInterface $result
    ): void
    {
        foreach ($this->sourceService->getValues($fileName) as $valuesBuffer) {
            $values = explode(';', trim($valuesBuffer));
            if (!$this->validateValues($values, $command, $log)) {
                continue;
            }
            array_push($values, $command->result((int)$values[0], (int)$values[1]));
            $result->setLine(implode(';', $values));
        }
    }

    /**
     * @param array $values
     * @param CommandInterface $command
     * @param SourceWritableInterface $log
     * @return bool
     */
    private function validateValues(array $values, CommandInterface $command, SourceWritableInterface $log): bool
    {
        $flag = true;

        if (!$this->isCorrectValues($values)) {
            $log->setLine(sprintf('set of numbers is wrong: %s', implode(';', $values)));
            $flag = false;
        }
        if (!$command->isValidValues($values[0], $values[1])) {
            $log->setLine($command->getMessage());
            $flag = false;
        }
        return $flag;
    }

    /**
     * @param array $values
     * @return bool
     */
    private function isCorrectValues(array $values): bool
    {
        return count($values) == 2 && $values[0] != 0 && $values[1] != 0;
    }
}