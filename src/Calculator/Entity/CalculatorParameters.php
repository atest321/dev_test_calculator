<?php
declare(strict_types=1);

namespace App\Calculator\Entity;

class CalculatorParameters
{
    public const PARAMETER_ACTION = ['a', 'action'];
    public const PARAMETER_FILE = ['f', 'file'];
}