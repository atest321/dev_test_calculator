<?php
declare(strict_types=1);

namespace App\Calculator\Helper;

class ConsoleParamsHelper
{
    /** @var array */
    private $options;

    /**
     * All options _must_ be parsed at once, otherwise the second option value is always lost
     * @param array $parameters
     */
    public function init(array $parameters): void
    {
        $shortList = '';
        $longList = [];

        foreach ($parameters as $names) {
            foreach ($names as $name) {
                if (strlen($name) === 1) {
                    $shortList .= $name . ':';
                } else {
                    $longList[] = $name . ':';
                }
            }
        }

        $this->options = getopt($shortList, $longList);
    }

    /**
     * @param array $names
     * @return string
     */
    public function getValueByParameterNames(array $names): string
    {
        $result = '';
        foreach ($names as $name) {
            if (isset($this->options[$name])) {
                $result = $this->options[$name];
                break;
            }
        }
        return $result;
    }
}