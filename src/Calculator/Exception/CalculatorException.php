<?php
declare(strict_types=1);

namespace App\Calculator\Exception;

use Exception;

class CalculatorException extends Exception
{

}