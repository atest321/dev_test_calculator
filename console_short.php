<?php
error_reporting(E_ALL);
ini_set('display_errors',  true);

// short version of calculator
define('ROOT', __DIR__ . DIRECTORY_SEPARATOR);
define('OUTPUT_DIR', ROOT . 'output' . DIRECTORY_SEPARATOR);

try {
    list($action, $file) = getParams();

    $sourceFile = fopen(ROOT . $file, 'r');
    $resultFile = fopen(OUTPUT_DIR . 'result_' . $file, 'w');
    $logFile = fopen(OUTPUT_DIR . 'log_' . $file, 'w');

    while ($buffer = fgets($sourceFile, 4096)) {
        $log = '';
        list($a, $b) = explode(';', trim($buffer));
        $result = calculate($a, $b, $action, $log);
        if ($log || $a == 0 || $b == 0 || $result < 0) {
            fputs($logFile, ($log ?: sprintf('set of numbers %d and %d is wrong', $a, $b)) . PHP_EOL);
        } else {
            fputs($resultFile, implode(';', [$a, $b, $result]) . PHP_EOL);
        }
    }

    fclose($sourceFile);
    fclose($resultFile);
    fclose($logFile);
    echo 'Calculations are done, please check the output folder';
} catch (Exception $e) {
    echo 'Error: ' . $e->getMessage();
}

/**
 * @return array
 * @throws Exception
 */
function getParams(): array
{
    $options = getopt('a:f:', ['action:', 'file:']);
    $action = $options['a'] ?? $options['action'] ?? '';
    $file = $options['f'] ?? $options['file'] ?? '';
    if (!$action) {
        throw new Exception('Action is not provided');
    }
    if (!$file) {
        throw new Exception('File is not provided');
    }
    return [$action, $file];
}

/**
 * @param int $a
 * @param int $b
 * @param string $action
 * @param string $log
 * @return float|int
 * @throws Exception
 */
function calculate(int $a, int $b, string $action, string &$log)
{
    switch ($action) {
        case 'plus':
            return $a + $b;
        case 'minus':
            return $a - $b;
        case 'multiply':
            return $a * $b;
        case 'division':
            if (!$b) {
                $log = sprintf('numbers %d and %d cannot be divided', $a, $b);
                return 0;
            }
            return $a / $b;
        default:
            throw new Exception('Unknown action: ' . $action);
    }
}


