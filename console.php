<?php

require __DIR__ . '/src/bootstrap.php';

define('ROOT', __DIR__ . DIRECTORY_SEPARATOR);

try {
    getCalculatorService()->calculate();

    echo PHP_EOL . 'Command complete. Please check the output files';
} catch (Exception $e) {
    echo $e->getMessage();
}

