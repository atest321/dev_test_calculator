# Neuffer developers-test

Script for calculating numbers and files and store them back.

Requirements: PHP 7.1+, Composer

Set up: `composer install`

The script is writing data to a folder `./output`, for proper work please provide necessary permissions, e.g. `chmod 777 ./output`

Start: `php console.php --action {action} --file {file}`

Script will take two required parameters:

`{file}` - csv-source file with numbers, where each row contains two numbers between -100 and 100, and

`{action}` - what action should we do with numbers from `{file}`, and can take next values:

* <b>plus</b> - to count summ of the numbers on each row in the {file}
* <b>minus</b> - to count difference between first number in the row and second
* <b>multiply</b> - to multiply the numbers on each row in the {file} 
* <b>division</b> - to divide  first number in the row and second

As result of the command execution should be csv file with three columns: first number, second number, and result. In CSV-file should be written **ONLY** numbers greater than null. If result less than null - it should be written in logs.

`php console.php --action division  --file {file}`, where in file you can find next numbers:

Test run: `./vendor/bin/phpunit --bootstrap vendor/autoload.php test`

### Is there anything shorter and simpler? ###

Yes. `php console_short.php --action {action} --file {file}` 

**Example**

Source file contents:
```
20;10
-30;20
3;0
```

Contents of CSV file:

`20;10;2`

Contents of log file:
 
 ```
 set of numbers -30 and 20 is wrong
 numbers 3 and 0 cannot be divided
```