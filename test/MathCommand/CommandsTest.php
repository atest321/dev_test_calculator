<?php
declare(strict_types=1);

namespace MathCommand;

use App\MathCommand\Entity\MinusEntity;
use App\MathCommand\Entity\PlusEntity;
use PHPUnit\Framework\TestCase;

class CommandsTest extends TestCase
{
    /**
     * @dataProvider plusDataProvider
     * @param int $firstValue
     * @param int $secondValue
     * @param int $expected
     */
    public function testPlus(int $firstValue, int $secondValue, int $expected) {
        $plusCommand = new PlusEntity();
        $this->assertEquals($expected, $plusCommand->result($firstValue, $secondValue));
    }

    public function plusDataProvider() {
        return [
            [1, 2, 3],
            [-1, 2, 1],
            [-1, 1, 0],
        ];
    }

    /**
     * @dataProvider minusDataProvider
     * @param int $firstValue
     * @param int $secondValue
     * @param int $expected
     */
    public function testMinus(int $firstValue, int $secondValue, int $expected) {
        $plusCommand = new MinusEntity();
        $this->assertEquals($expected, $plusCommand->result($firstValue, $secondValue));
    }

    public function minusDataProvider() {
        return [
            [2, 1, 1],
            [1, 2, -1],
            [-1, 2, -3],
            [-1, 1, -2],
        ];
    }
}